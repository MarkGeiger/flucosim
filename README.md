# FluxControlSim

**FluCoSim** (Flux Control Simulator) is an open source software tool to verify aimpoint distribution and flux control algorithms for heliostat based 
power plants. 

The complete project is licensed under the Apache2 license and thus free to use for anybody. 
For more information please refer to the projects documentation, which can be found here:

https://gitlab.com/MarkGeiger/flucosim/blob/master/documentation/documentation.adoc
