#include <iostream>
#include "sim/MainLoop.h"

int main() {
    MainLoop loop{};
    loop.start();
    return 0;
}
