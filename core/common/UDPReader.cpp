#include "UDPReader.h"

void UDPReader::init(const std::string &ip, uint16_t udp_port)
{
    struct sockaddr_in cliaddr{};

    // Creating socket file descriptor
    if ( (sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
        perror("socket creation failed");
        exit(EXIT_FAILURE);
    }

    memset(&servaddr, 0, sizeof(servaddr));
    memset(&cliaddr, 0, sizeof(cliaddr));

    // Filling server information
    servaddr.sin_family    = AF_INET; // IPv4
    servaddr.sin_addr.s_addr = INADDR_ANY;
    servaddr.sin_port = htons(udp_port);

    // Bind the socket with the server address
    if (bind(sockfd, (const struct sockaddr *)&servaddr,
              sizeof(servaddr)) < 0 )
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

}

bool UDPReader::read()
{
    struct sockaddr_in cliaddr{};
    memset(&cliaddr, 0, sizeof(cliaddr));
    int len;
    count = recvfrom(sockfd, buffer, 1024, MSG_WAITALL, reinterpret_cast<sockaddr *>(&cliaddr), reinterpret_cast<socklen_t *>(&len));
    return count >= 0;
}

void UDPReader::close()
{

}

char *UDPReader::getBuffer()
{
    return buffer;
}

ssize_t UDPReader::getCount() const
{
    return count;
}

