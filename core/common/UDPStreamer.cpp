#include "UDPStreamer.h"

void UDPStreamer::init(const std::string &ip, uint16_t udp_port)
{
    // Creating socket file descriptor
    if ( (sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
        perror("socket creation failed");
        exit(EXIT_FAILURE);
    }

    // Filling server information
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(udp_port);
    servaddr.sin_addr.s_addr = inet_addr(ip.c_str());
}

void UDPStreamer::stream(uint32_t* data, size_t size)
{
    sendto(sockfd, data, size, MSG_CONFIRM, (const struct sockaddr *) &servaddr, sizeof(servaddr));
}

void UDPStreamer::close()
{
    shutdown(sockfd, SHUT_WR);
}


void UDPStreamer::streamDetection(int numOfTargets, double* targetX, double* targetY)
{
    int id = 5;
    sendto(sockfd, &id, sizeof(int), MSG_MORE, (const struct sockaddr *) &servaddr, sizeof(servaddr));

    if (numOfTargets == 0)
    {
        sendto(sockfd, &numOfTargets, sizeof(int), MSG_CONFIRM, (const struct sockaddr *) &servaddr, sizeof(servaddr));
        return;
    }
    // send dimensions
    sendto(sockfd, &numOfTargets, sizeof(int), MSG_MORE, (const struct sockaddr *) &servaddr, sizeof(servaddr));

    // send payload
    size_t size = numOfTargets * sizeof(double);
    sendto(sockfd, targetX, size, MSG_MORE, (const struct sockaddr *) &servaddr, sizeof(servaddr));
    sendto(sockfd, targetY, size, MSG_CONFIRM, (const struct sockaddr *) &servaddr, sizeof(servaddr));
}

void UDPStreamer::streamFlux(int fluxID, double* flux, int binx, int biny)
{
    size_t size = binx * biny * sizeof(double);

    // send message id
    int id = fluxID;
    sendto(sockfd, &id, sizeof(int), MSG_MORE, (const struct sockaddr *) &servaddr, sizeof(servaddr));

    // send dimensions
    sendto(sockfd, &binx, sizeof(int), MSG_MORE, (const struct sockaddr *) &servaddr, sizeof(servaddr));
    sendto(sockfd, &biny, sizeof(int), MSG_MORE, (const struct sockaddr *) &servaddr, sizeof(servaddr));

    // send payload
    sendto(sockfd, flux, size, MSG_CONFIRM, (const struct sockaddr *) &servaddr, sizeof(servaddr));
}
