#ifndef CORE_UDPREADER_H
#define CORE_UDPREADER_H

#include <sys/types.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <memory.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <errno.h>
#include <stdlib.h>
#include <iostream>
#include <vector>

class UDPReader
{

private:
    int sockfd{};
    struct sockaddr_in servaddr{};

    char buffer[1024];
    ssize_t count;

public:
    UDPReader() = default;
    ~UDPReader() = default;

    void init(const std::string &ip, uint16_t udp_port);
    void close();
    bool read();

    char *getBuffer();
    ssize_t getCount() const;
};


#endif //CORE_UDPREADER_H
