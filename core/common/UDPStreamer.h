#ifndef _OutStream_HPP_
#define _OutStream_HPP_


#include <sys/types.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <memory.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <errno.h>
#include <stdlib.h>
#include <iostream>
#include <vector>

class UDPStreamer {

private:
    int sockfd{};
    struct sockaddr_in servaddr{};

public:
    UDPStreamer() = default;
    ~UDPStreamer() = default;

    void init(const std::string &ip, uint16_t udp_port);
    void stream(uint32_t* data, size_t size);
    void streamFlux(int fluxID, double* flux, int binx, int biny);
    void streamDetection(int numOfTargets, double* targetX, double* targetY);
    void close();

};
#endif // _OutStream_HPP_
