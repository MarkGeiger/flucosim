#ifndef CONFIGPARSER_H
#define CONFIGPARSER_H

#include <string>
#include <map>
#include <vector>

/**
 * Parses Configuration files
 */
class ConfigParser
{

private:
    /**
     * Stores entire configuration in string based format. File structure is loaded by "parseFile"
     * <section, <key, value>>
     */
    std::map<std::string, std::map<std::string, std::string>> config;

    void
    store(std::string section, std::string key, std::string value);

public:
    ConfigParser() = default;

    /**
     * Pareses config file and stores key value pairs in internal data structures.
     * Throws illegal argument exception if config file cannot be read.
     *
     * @param path to the configuration file
     */
    void
    parseFile(std::string path);

    void
    printConfig();

    double
    getValueAsDouble(std::string section, std::string key);

    bool
    getValueAsBoolean(std::string section, std::string key);

    int
    getValueAsInteger(std::string section, std::string key);

    std::string&
    getValueAsString(std::string section, std::string key);

    bool
    getValueAsDoubleArray(std::string section, std::string key, std::vector<double> &val);
};

#endif //CONFIGPARSER_H
