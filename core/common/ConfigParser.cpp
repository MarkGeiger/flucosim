#include <utility>
#include <sstream>
#include <fstream>
#include <vector>
#include <iostream>
#include <algorithm>
#include "ConfigParser.h"

/**
 * Helper method to trim whitespaces
 *
 * @param str
 * @param whitespace
 * @return
 */
std::string trim(const std::string& str,
                 const std::string& whitespace = " \t")
{
    const auto strBegin = str.find_first_not_of(whitespace);
    if (strBegin == std::string::npos)
        return ""; // no content

    const auto strEnd = str.find_last_not_of(whitespace);
    const auto strRange = strEnd - strBegin + 1;

    return str.substr(strBegin, strRange);
}

void
ConfigParser::store(std::string section, std::string key, std::string value)
{
    if(config.find(section) == config.end())
    {
        std::map < std::string, std::string > inner;
        inner.insert(std::make_pair(key, value));
        config.insert(std::make_pair(section, inner));
    }
    else
    {
        config[section][key] = std::move(value);
    }
}

void
ConfigParser::printConfig()
{
    std::cout << "------------- Start -----------------\n";
    for(std::map<std::string, std::map<std::string, std::string> >::const_iterator ptr =
            config.begin(); ptr != config.end(); ptr++)
    {
        std::cout << ptr->first << ":\n";
        for(const auto &eptr : ptr->second)
        {
            std::cout << "\t" << eptr.first << " " << eptr.second << std::endl;
        }
    }
    std::cout << "------------- End -------------------\n";
}

void
ConfigParser::parseFile(std::string path)
{
    std::ifstream file(path);
    if(!file)
    {
        throw std::invalid_argument("Could not find config file");
    }

    file.seekg(0, std::ios::end);
    std::streampos length = file.tellg();
    file.seekg(0, std::ios::beg);

    std::vector<char> buffer(static_cast<unsigned long>(length));
    file.read(&buffer[0], length);

    std::stringstream is_file;
    is_file.rdbuf()->pubsetbuf(&buffer[0], length);

    std::string line;
    std::string section = "undefined";
    while(std::getline(is_file, line))
    {
        // ignore empty lines
        if(line.empty())
            continue;

        // this line is a comment, ignore it
        if('#' == line[0])
            continue;

        std::string trimmed_line = trim(line);
        if('#' == trimmed_line[0])
            continue;

        std::string key;
        std::istringstream is_line(line);
        if(std::getline(is_line, key, '='))
        {
            // remove spaces and tabs from key
            key.erase(remove_if(key.begin(), key.end(), isspace), key.end());

            std::string value;
            if (std::getline(is_line, value))
            {
                store(section, key, trim(value));
            }
        }

        if('!' == line[0])
        {
            section = trim(line.substr(1, line.size() - 1));
        }
    }
}

double
ConfigParser::getValueAsDouble(std::string section, std::string key)
{
    std::string val = config[section][key];
    return std::stod(val);
}

bool
ConfigParser::getValueAsBoolean(std::string section, std::string key)
{
    std::string val = config[section][key];
    std::transform(val.begin(), val.end(), val.begin(), ::tolower);
    return val == "true" || val == "1";
}

int
ConfigParser::getValueAsInteger(std::string section, std::string key)
{
    std::string val = config[section][key];
    return std::stoi(val);
}

std::string&
ConfigParser::getValueAsString(std::string section, std::string key)
{
    return config[section][key];
}

bool
ConfigParser::getValueAsDoubleArray(std::string section, std::string key, std::vector<double> &val)
{
    std::string s_val = config[section][key];
    std::size_t pos = 0;
    while (pos < s_val.size ())
        if ((pos = s_val.find_first_of (',', pos)) != std::string::npos)
            s_val[pos] = ' ';

    double d = 0.0;
    std::stringstream ss(s_val);
    while (ss >> d)
        val.push_back (d);
    return true;
}
