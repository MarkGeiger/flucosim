#ifndef CORE_POWERESTIMATIONTEST_H
#define CORE_POWERESTIMATIONTEST_H

#include "gtest/gtest.h"
#include "../sim/PowerEstimation.h"
#include "../model/Vector3.h"
#include "../model/Vector2.h"
#include "../model/Receiver.h"
#include "../model/Heliostat.h"

class PowerEstimationTest : public ::testing::Test
{

protected:

    PowerEstimationTest()
    {
    }

    ~PowerEstimationTest() override
    {
    }

    void SetUp() override
    {
        Test::SetUp();
    }

    void TearDown() override
    {
        Test::TearDown();
    };

};

TEST_F(PowerEstimationTest, TestHeliostatEfficiencyCalculation)
{
    PowerEstimation estimation{};

    Receiver receiver{Vector3(0,0,60)};
    receiver.setWidth(10);
    receiver.setLength(10);
    receiver.initialize();

    Heliostat heliostatBadAngleAndFar{Vector3(0,500,5), Vector2(0,0)};
    Heliostat heliostatMid{Vector3(0,100,5), Vector2(0,0)};
    Heliostat heliostatGoodAngle{Vector3(0,10,5), Vector2(0,0)};

    Vector3 sunVector(0,0,1);

    double efficiencyLow = estimation.estimateHeliostatEfficiency(sunVector, heliostatBadAngleAndFar, receiver);
    double efficiencyMid = estimation.estimateHeliostatEfficiency(sunVector, heliostatMid, receiver);
    double efficiencyHigh = estimation.estimateHeliostatEfficiency(sunVector, heliostatGoodAngle, receiver);

    ASSERT_LE(efficiencyMid, efficiencyHigh);
    ASSERT_LE(efficiencyLow, efficiencyMid);
}

#endif //CORE_POWERESTIMATIONTEST_H
