#ifndef CORE_MAINLOOP_H
#define CORE_MAINLOOP_H


#include "SimControl.h"

class MainLoop
{

private:
    bool running{false};
    SimControl simControl{};


public:
    void start();

};


#endif //CORE_MAINLOOP_H