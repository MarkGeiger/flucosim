#include <unistd.h>
#include <cstdio>
#include <iostream>
#include <chrono>
#include <math.h>
#include <algorithm>
#include "MainLoop.h"

#define UPDATES_IN_HZ 10.0
#define FRAME_TIME 1.0/UPDATES_IN_HZ

void MainLoop::start()
{
    simControl.init();
    running = true;
    while (this->running)
    {
        auto start = std::chrono::steady_clock::now();

        // do stuff here
        simControl.control();

        // estimate waiting time to keep static frame rate
        auto end = std::chrono::steady_clock::now();
        auto dif = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
        double timeToWait = std::max(1.0, FRAME_TIME*1000.0*1000.0 - dif);

        usleep(static_cast<__useconds_t>((long) timeToWait));

        // print frame actual frame time
        end = std::chrono::steady_clock::now();
        dif = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
        std::cout << "Elapsed time in microseconds : "
                  << dif
                  << " µs" << " left: " << timeToWait << std::endl;
    }
}
