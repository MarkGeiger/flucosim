#include "PowerEstimation.h"

#include <math.h>
#include <cstdio>
#include <algorithm>

Vector3 calcHeliostatNormalVector(const Heliostat &heliostat, Vector3 &sunVector, Vector3 &target)
{
    Vector3 heliostatToTargetNormalized = target.subtract(heliostat.getPos()).normalize();
    return sunVector.add(heliostatToTargetNormalized.multiply(2.0)).normalize();
}
// test
double PowerEstimation::estimateHeliostatEfficiency(Vector3 &sunVector, const Heliostat &heliostat, const Receiver &receiver)
{
    Vector3 position = heliostat.getPos();
    Vector3 target = receiver.getPos();

    // Ray vector from heliostat to AimPoint
    Vector3 rayFull = target.add(position.multiply(-1.0));
    Vector3 ray = rayFull.normalize();
    Vector3 normalVector = calcHeliostatNormalVector(heliostat, sunVector, target);

    // efficiency = shading * cos * reflectivity * blocking * transmission * intercept
    double cosine = normalVector.dot(ray); // cos for normalized vectors
    double etaShade = 1.0; // no shading
    double etaBlock = 1.0; // no blocking
    double transmission = 0.99326 - 1.046e-4 * rayFull.length() + 1.7e-8 * pow(rayFull.length(), 2)
                          - 2.845e-12 * pow(rayFull.length(), 3);
    double areaAperture = receiver.getLength() * receiver.getWidth();
    double imageDiameter = rayFull.length() * 0.015; // image has diameter of 1.5 m in 100 m distance,
    double imageArea = M_PI * pow(imageDiameter/2.0, 2); // assumer circular spot
    // area as square of diameter

    double etaIntercept;
    if (imageArea < areaAperture)
    {
        etaIntercept = 1;
    } else
    {
        etaIntercept = areaAperture / imageArea;
    }

    return etaShade * cosine * heliostat.getReflectivity() * etaBlock * transmission * etaIntercept;
}

