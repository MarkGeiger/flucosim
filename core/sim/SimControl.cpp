#include <cstdio>
#include <cmath>
#include <chrono>
#include <random>
#include <algorithm>
#include "SimControl.h"
#include "../common/UDPReader.h"

SimControl::SimControl() = default;

int getInt(const char *buffer, int offset)
{
    return int((unsigned char) (buffer[offset]) << 24 |
               (unsigned char) (buffer[offset + 1]) << 16 |
               (unsigned char) (buffer[offset + 2]) << 8 |
               (unsigned char) (buffer[offset + 3]));
}

void SimControl::receiverTask()
{
    UDPReader reader{};
    reader.init("127.0.0.1", 6004);
    while (running)
    {
        if (reader.read())
        {
            // receive a paket
            char *buffer = reader.getBuffer();
            int id = getInt(buffer, 0);

            if (id == 1)
            {
                std::random_device rd;
                std::mt19937 mt(rd());
                std::uniform_real_distribution<double> distX(0.0, field.getReceiver().getBinX());
                std::uniform_real_distribution<double> distY(0.0, field.getReceiver().getBinY());

                // shuffle command
                int numToShuffle = std::min(getInt(buffer, 4), (int) field.getHeliostats().size());
                for (int i = 0; i < numToShuffle; i++)
                {
                    field.getHeliostats().at(i).setTargetSet(Vector2(distX(mt), distY(mt)));
                }
            }
        } else
        {
            // some error during receive
            usleep(100);
        }
    }
    reader.close();
}

void SimControl::init()
{
    running = true;
    streamer.init("127.0.0.1", 6003);
    oldTime = std::chrono::steady_clock::now();
    thread = new std::thread(&SimControl::receiverTask, this);

    model.setInitD(50);
    //model.useMeta(true);
    /* Set init_alpha to 250 in all elements */
    model.setInitAlpha(0);
    /* Set w_gen to 0.2 */
    model.wGen(0.3);
    model.finalLambda(0.99999);
    model.tauLambda(0.99999);

    doubleVec norm(3, 15);
    model.normIn(norm);
}

void SimControl::control()
{
    auto newTime = std::chrono::steady_clock::now();
    double dt = (newTime - oldTime).count() / 1e9; // dt in s
    oldTime = newTime;

    auto flux = field.getReceiver().getFlux();
    Vector3 sun = field.getSunVector();
    double dni = 900 * 5;
    double cellw = field.getReceiver().getLength() / field.getReceiver().getBinX();
    double cellh = field.getReceiver().getWidth() / field.getReceiver().getBinY();
    double cellArea = cellw * cellh;
    for (int i = 0; i < field.getReceiver().getBinY(); i++)
    {
        for (int j = 0; j < field.getReceiver().getBinX(); j++)
        {
            flux[j + i * field.getReceiver().getBinX()] = 0;
        }
    }

    for (Heliostat &heliostat : field.getHeliostats())
    {
        double eff = estimator.estimateHeliostatEfficiency(sun, heliostat, field.getReceiver());
        double totalPower = eff * dni * heliostat.getArea();
        const Vector3 &pos = heliostat.getPos();
        Vector3 target = field.getReceiver().getPos();
        double imageDiameter = target.subtract(pos).length() * 0.035;
        int coveredCellsW = static_cast<int>(lround((imageDiameter / cellw) / 2.0));
        int coveredCellsH = static_cast<int>(lround((imageDiameter / cellh) / 2.0));
        double maxcCellDist = sqrt(pow(coveredCellsH, 2) + pow(coveredCellsW, 2));
        int coveredCells = coveredCellsW * coveredCellsH;
        double avgPowerPerCell = totalPower / coveredCells;
        for (int i = (int) -maxcCellDist; i <= maxcCellDist; i++)
        {
            for (int j = (int) -maxcCellDist; j <= maxcCellDist; j++)
            {
                int tx = static_cast<int>(lround(heliostat.getTarget().getX()));
                int ty = static_cast<int>(lround(heliostat.getTarget().getY()));
                if (tx + j >= field.getReceiver().getBinX() ||
                    ty + i >= field.getReceiver().getBinY() ||
                    tx + j < 0 ||
                    ty + i < 0)
                {
                    // intercept power loss
                    continue;
                }
                int combs[1] = {tx + j + (ty + i) * field.getReceiver().getBinX()};
                double cellDistance = sqrt(pow(j, 2) + pow(i, 2));

                // use rayleigh function instead of linear approx.
                double distanceFac = std::max(0.0, 1 - cellDistance / maxcCellDist);

                // * 2 because of the reduction through the distanceFac, the overall accumulated power should be equal to
                // totalPower. So this is not 100% correct, its a rough estimation.
                flux[combs[0]] += ((avgPowerPerCell / cellArea) / 1000.0) * distanceFac * 2; // kw/m^2
            }
        }
        heliostat.updateHeliostatMovement(dt, cellw, cellh);
    }

    // add noise to flux
    double noise = 1;
    std::normal_distribution<double> distribution(0.0, 1.0);
    for (int i = 0; i < field.getReceiver().getBinY(); i++)
    {
        for (int j = 0; j < field.getReceiver().getBinX(); j++)
        {
            // amount of noise should depend an cell size
            double number = distribution(generator) * noise;
            flux[j + i * field.getReceiver().getBinX()] += number;
        }
    }

    // add here
    {
        // power / temperature estimation

        // power control

        // flux control
    }


    // stream flux data to GUI
    streamer.streamFlux(1, field.getReceiver().getFlux(), field.getReceiver().getBinX(), field.getReceiver().getBinY());

    // calculate flux dif and history
    field.getReceiver().calcFluxDif();

    double maxFlux = 0;
    int idX = -1;
    int idY = -1;
    for (int i = 0; i < field.getReceiver().getBinY(); i++)
    {
        for (int j = 0; j < field.getReceiver().getBinX(); j++)
        {
            double val = field.getReceiver().getDifFlux()[j + i * field.getReceiver().getBinX()];
            if (val > 10)
            {
                field.getReceiver().getDetFlux()[idX + idY * field.getReceiver().getBinX()] = val;
            }
            if (val > maxFlux && val > 10)
            {
                maxFlux = val;
                idX = j;
                idY = i;
            } else if (val < 10)
            {
                field.getReceiver().getDetFlux()[j + i * field.getReceiver().getBinX()] = 0;
            }
        }
    }
    /*
    if (idX != -1)
    {
        field.getReceiver().getDetFlux()[idX + idY * field.getReceiver().getBinX()] = maxFlux;
        if (idX > 0)
            field.getReceiver().getDetFlux()[idX - 1 + idY * field.getReceiver().getBinX()] = maxFlux;
        if (idX + 1 < field.getReceiver().getBinX())
            field.getReceiver().getDetFlux()[idX + 1 + idY * field.getReceiver().getBinX()] = maxFlux;
        if (idX > 0)
            field.getReceiver().getDetFlux()[idX + (idY - 1) * field.getReceiver().getBinX()] = maxFlux;
        if (idY + 1 < field.getReceiver().getBinY())
            field.getReceiver().getDetFlux()[idX + (idY + 1) * field.getReceiver().getBinX()] = maxFlux;
    }*/

    if (field.getReceiver().isFullyInitialized())
    {
        for (int i = 0; i < field.getReceiver().getBinY(); i++)
        {
            for (int j = 0; j < field.getReceiver().getBinX(); j++)
            {
                double val = field.getReceiver().getDetFlux()[j + i * field.getReceiver().getBinX()];
                if (val > 0)
                {
                    doubleVec x(3);
                    x[0] = idX;
                    x[1] = idY;
                    x[2] = stamp;
                    doubleVec y(1);
                    // val should decide classification
                    y[0] = 20;
                    model.update(x, y);
                }
            }
        }
    }
    stamp++;

    for (int i = 0; i < field.getReceiver().getBinY(); i++)
    {
        for (int j = 0; j < field.getReceiver().getBinX(); j++)
        {
            doubleVec x(3);
            x[0] = j;
            x[1] = i;
            x[2] = stamp - 5;
            field.getReceiver().getModelFlux()[j + i * field.getReceiver().getBinX()] = model.predict(x, 0.001)[0];
        }
    }

    long sumX = 0;
    long sumY = 0;
    int num = 0;
    // he some grouping could be done with kMeans
    for (int i = 0; i < field.getReceiver().getBinY(); i++)
    {
        for (int j = 0; j < field.getReceiver().getBinX(); j++)
        {
            double val = field.getReceiver().getModelFlux()[j + i * field.getReceiver().getBinX()];
            if (val > 0)
            {
                sumX += j;
                sumY += i;
                num++;
            }
        }
    }

    if (num != 0)
    {
        double posX = sumX / num;
        double posY = sumY / num;
        streamer.streamDetection(1, &posX, &posY);
    } else {
        streamer.streamDetection(0, nullptr, nullptr);
    }

    streamer.streamFlux(3, field.getReceiver().getDetFlux(), field.getReceiver().getBinX(),
                        field.getReceiver().getBinY());

    streamer.streamFlux(4, field.getReceiver().getModelFlux(), field.getReceiver().getBinX(),
                        field.getReceiver().getBinY());

    // stream dif flux data to GUI
    streamer.streamFlux(2, field.getReceiver().getDifFlux(), field.getReceiver().getBinX(),
                        field.getReceiver().getBinY());

}

