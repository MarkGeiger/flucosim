#ifndef CORE_POWERESTIMATION_H
#define CORE_POWERESTIMATION_H

#include "../model/Heliostat.h"
#include "../model/Receiver.h"
#include "../model/Vector3.h"

class PowerEstimation
{

public:
    double estimateHeliostatEfficiency(Vector3 &sunVector, const Heliostat &heliostat, const Receiver &receiver);
};


#endif //CORE_POWERESTIMATION_H
