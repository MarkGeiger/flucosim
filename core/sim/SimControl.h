#ifndef CORE_SIMCONTROL_H
#define CORE_SIMCONTROL_H


#include "../model/Field.h"
#include "PowerEstimation.h"
#include "../common/UDPStreamer.h"
#include <thread>
#include <chrono>
#include <random>
#include <lwpr.hh>

class SimControl
{
private:
    bool running;
    Field field{};
    PowerEstimation estimator{};
    UDPStreamer streamer{};
    std::default_random_engine generator{};
    std::chrono::time_point<std::chrono::_V2::steady_clock, std::chrono::duration<int64_t, std::ratio<1, 1000000000>>> oldTime;
    std::thread* thread;
    LWPR_Object model{3,1};
    double stamp = 0;

    void receiverTask();

public:
    SimControl();
    void init();
    void control();
};


#endif //CORE_SIMCONTROL_H