file(GLOB SOURCE_FILES *.cpp)
file(GLOB HEADER_FILES *.h)

add_library(sim STATIC ${SOURCE_FILES} ${HEADER_FILES})
target_link_libraries(sim model common ${CMAKE_THREAD_LIBS_INIT})