#include "Vector2.h"
#include <math.h>

Vector2::Vector2(double x, double y) : x(x), y(y)
{}

Vector2::Vector2(const Vector2 &copy)
{
    this->x = copy.x;
    this->y = copy.y;
}

Vector2 Vector2::add(Vector2 &vec)
{
    return Vector2(vec.x + this->x, vec.y + this->y);
}

Vector2 Vector2::multiply(double scalar)
{
    return Vector2(this->x * scalar,this->y * scalar);
}

double Vector2::length()
{
    return sqrt(this->x * this->x + this->y * this->y);
}

double Vector2::getX() const
{
    return x;
}

double Vector2::getY() const
{
    return y;
}

Vector2 Vector2::subtract(Vector2 &vec)
{
    return Vector2(this->x - vec.x, this->y - vec.y);
}

Vector2 Vector2::normalize()
{
    double m = 1/length();
    return Vector2(this->x * m, this->y * m);
}
