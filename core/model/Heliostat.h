#ifndef CORE_HELIOSTAT_H
#define CORE_HELIOSTAT_H

#include "Vector3.h"
#include "Vector2.h"

class Heliostat
{

private:
    Vector3 pos;
    Vector2 target;
    Vector2 targetSet;

public:
    Heliostat(const Vector3 &pos, const Vector2 &target);

    const Vector3 &getPos() const;
    const Vector2 &getTarget() const;
    void setTarget(const Vector2 &target);

    const Vector2 &getTargetSet() const;
    void setTargetSet(const Vector2 &targetSet);

    double getReflectivity() const;
    double getArea() const;

    void updateHeliostatMovement(double dt, double cellw, double cellh);
};


#endif //CORE_HELIOSTAT_H