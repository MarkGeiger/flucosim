#include "Vector3.h"
#include <math.h>

Vector3::Vector3(double x, double y, double z) : x(x), y(y), z(z)
{}

Vector3::Vector3(const Vector3 &copy)
{
    this->x = copy.x;
    this->y = copy.y;
    this->z = copy.z;
}

Vector3 Vector3::add(const Vector3 &vec)
{
    return Vector3(vec.x + this->x, vec.y + this->y, vec.z + this->z);
}

Vector3 Vector3::subtract(const Vector3 &vec)
{
    return Vector3(this->x - vec.x, this->y - vec.y, this->z - vec.z);
}

Vector3 Vector3::multiply(double scalar)
{
    return Vector3(this->x * scalar,this->y * scalar, this->z * scalar);
}

Vector3 Vector3::normalize()
{
    double m = 1/length();
    return Vector3(this->x * m, this->y * m, this->z * m);
}

double Vector3::length()
{
    return sqrt(this->x * this->x + this->y * this->y + this->z * this->z);
}

double Vector3::getX() const
{
    return x;
}

double Vector3::getY() const
{
    return y;
}

double Vector3::getZ() const
{
    return z;
}

double Vector3::dot(const Vector3 &vec)
{
    return vec.x * this->x + vec.y * this->y + vec.z * this->z;
}

