#ifndef CORE_RECEIVER_H
#define CORE_RECEIVER_H

#include "Vector3.h"
#include <vector>

class Receiver
{

private:
    Vector3 pos{0,0,0};
    double massFlow{};
    double tempIn{};
    double tempOut{};
    double heatCapacity{};
    double width{};
    double length{};
    double absorptivity{};
    int binX{};
    int binY{};

    double *flux = nullptr;
    double *det_flux = nullptr;
    double *model_flux = nullptr;
    double *dif_flux = nullptr;
    std::vector<double*> old_flux_maps{};
    unsigned long currentOldFluxIndicator = 0;

    bool fullyInitialized = false;

    double simulatePowerLoss();
    double simulatePowerHTF();

public:
    explicit Receiver(const Vector3 &pos);
    ~Receiver();

    const Vector3 &getPos() const;

    double getMassFlow() const;
    void setMassFlow(double massFlow);

    double getTempIn() const;
    void setTempIn(double tempIn);

    double getTempOut() const;
    void setTempOut(double tempOut);

    double getHeatCapacity() const;
    void setHeatCapacity(double heatCapacity);

    double getWidth() const;
    void setWidth(double width);

    double getLength() const;
    void setLength(double length);

    double getAbsorptivity() const;
    void setAbsorptivity(double absorptivity);

    int getBinX() const;
    void setBinX(int binX);

    int getBinY() const;
    void setBinY(int binY);

    bool isFullyInitialized() const;

    double *getFlux() const;
    double *getOldFlux() const;
    double *getDifFlux() const;
    double *getDetFlux() const;
    double *getModelFlux() const;

    void initialize();
    void calcFluxDif();
    void simulate(double timeDifS, double powerIn);
};


#endif //CORE_RECEIVER_H