#include <ctime>
#include <cstdlib>
#include <random>
#include "Field.h"

#include "Vector2.h"
#include "Vector3.h"

#define REC_POS_X 0
#define REC_POS_Y 0
#define REC_POS_Z 60

Field::Field() : receiver(Vector3{REC_POS_X, REC_POS_Y, REC_POS_Z})
{
    generateReceiver();
    generateHeliostats();
}

void Field::generateReceiver()
{
    receiver.setAbsorptivity(0.9);
    receiver.setHeatCapacity(2000);
    receiver.setLength(10);
    receiver.setWidth(10);
    receiver.setMassFlow(1);
    receiver.setTempIn(600);
    receiver.setTempOut(700);
    receiver.setBinX(70);
    receiver.setBinY(70);
    receiver.initialize();
}

void Field::generateHeliostats()
{
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_real_distribution<double> dist(0.0, 75.0);

    int row = 1;
    for (int i = 0; i < 200; i++)
    {
        if (i % 100 == 0)
        {
            row++;
        }
        Heliostat heliostat{Vector3{static_cast<double>(50 - (i % 100)) , row * 5.0, 0},
                            Vector2{dist(mt), dist(mt)}
        };
        heliostats.push_back(heliostat);
    }
}

std::vector<Heliostat> &Field::getHeliostats()
{
    return heliostats;
}

Receiver &Field::getReceiver()
{
    return receiver;
}

const Vector3 &Field::getSunVector() const
{
    return sunVector;
}


