#ifndef CORE_VECTOR3_H
#define CORE_VECTOR3_H


class Vector3
{

private:
    double x;
    double y;
    double z;

public:
    Vector3(double x, double y, double z);
    Vector3(const Vector3 &copy);

    Vector3 add(const Vector3 &vec);
    Vector3 subtract(const Vector3 &vec);
    Vector3 multiply(double scalar);
    Vector3 normalize();
    double dot(const Vector3 &vec);
    double length();

    double getX() const;
    double getY() const;
    double getZ() const;
};


#endif //CORE_VECTOR3_H
