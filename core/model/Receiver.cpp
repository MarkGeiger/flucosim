#include "Receiver.h"

#define FLUX_HISTORY_SIZE 10

Receiver::Receiver(const Vector3 &pos) : pos(pos)
{}

double Receiver::getMassFlow() const
{
    return massFlow;
}

void Receiver::setMassFlow(double massFlow)
{
    Receiver::massFlow = massFlow;
}

double Receiver::getTempIn() const
{
    return tempIn;
}

void Receiver::setTempIn(double tempIn)
{
    Receiver::tempIn = tempIn;
}

double Receiver::getTempOut() const
{
    return tempOut;
}

void Receiver::setTempOut(double tempOut)
{
    Receiver::tempOut = tempOut;
}

double Receiver::getHeatCapacity() const
{
    return heatCapacity;
}

void Receiver::setHeatCapacity(double heatCapacity)
{
    Receiver::heatCapacity = heatCapacity;
}

double Receiver::getWidth() const
{
    return width;
}

void Receiver::setWidth(double width)
{
    Receiver::width = width;
}

double Receiver::getLength() const
{
    return length;
}

void Receiver::setLength(double length)
{
    Receiver::length = length;
}

double Receiver::getAbsorptivity() const
{
    return absorptivity;
}

void Receiver::setAbsorptivity(double absorptivity)
{
    Receiver::absorptivity = absorptivity;
}

void Receiver::simulate(double timeDifS, double power)
{

}

const Vector3 &Receiver::getPos() const
{
    return pos;
}

void Receiver::initialize()
{
    flux = new double[binX * binY];
    det_flux = new double[binX * binY];
    dif_flux = new double[binX * binY];
    model_flux = new double[binX * binY];
    for (int j = 0; j < binY; j++)
    {
        for (int i = 0; i < binX; i++)
        {
            flux[i + j * binX] = 0;
            dif_flux[i + j * binX] = 0;
            det_flux[i + j * binX] = 0;
            model_flux[i + j * binX] = 0;
        }
    }

    for (int i = 0; i < FLUX_HISTORY_SIZE; i++)
    {
        auto * old_flux = new double[binX * binY];
        old_flux_maps.push_back(old_flux);
    }
}

Receiver::~Receiver()
{
    delete[] flux;
    delete[] dif_flux;
    delete[] det_flux;
    delete[] model_flux;
    for (auto old : old_flux_maps)
    {
        delete[] old;
    }
    old_flux_maps.clear();
}

double *Receiver::getFlux() const
{
    return flux;
}

int Receiver::getBinX() const
{
    return binX;
}

void Receiver::setBinX(int binX)
{
    Receiver::binX = binX;
}

int Receiver::getBinY() const
{
    return binY;
}

void Receiver::setBinY(int binY)
{
    Receiver::binY = binY;
}

double *Receiver::getOldFlux() const
{
    return old_flux_maps.at(currentOldFluxIndicator);
}

double *Receiver::getDifFlux() const
{
    return dif_flux;
}

double *Receiver::getDetFlux() const
{
    return det_flux;
}

double *Receiver::getModelFlux() const
{
    return model_flux;
}

int counter = 0;

void Receiver::calcFluxDif()
{
    // calculating dif
    for (int j = 0; j < binY; j++)
    {
        for (int i = 0; i < binX; i++)
        {
            dif_flux[i + j * binX] = (flux[i + j * binX] - old_flux_maps.at((currentOldFluxIndicator + 1)%FLUX_HISTORY_SIZE)[i + j * binX]);
        }
    }
    counter++;
    if (counter >= FLUX_HISTORY_SIZE*2)
    {
        fullyInitialized = true;
    }

    currentOldFluxIndicator++;
    currentOldFluxIndicator = currentOldFluxIndicator % FLUX_HISTORY_SIZE;

    // swapping pointers
    flux = old_flux_maps.at(currentOldFluxIndicator);
}

bool Receiver::isFullyInitialized() const
{
    return fullyInitialized;
}
