#ifndef CORE_VECTOR2_H
#define CORE_VECTOR2_H


class Vector2
{

private:
    double x;
    double y;

public:
    Vector2(double x, double y);
    Vector2(const Vector2 &copy);

    Vector2 add(Vector2 &vec);
    Vector2 multiply(double scalar);
    Vector2 subtract(Vector2 &vec);
    Vector2 normalize();
    double length();

    double getX() const;
    double getY() const;
};


#endif //CORE_VECTOR2_H
