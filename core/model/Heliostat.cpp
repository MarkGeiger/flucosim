#include <algorithm>
#include "Heliostat.h"

Heliostat::Heliostat(const Vector3 &pos, const Vector2 &target) : pos(pos), target(target), targetSet(target)
{}

const Vector2 &Heliostat::getTarget() const
{
    return target;
}

const Vector3 &Heliostat::getPos() const
{
    return pos;
}

double Heliostat::getReflectivity() const
{
    return 0.95;
}

double Heliostat::getArea() const
{
    return 4*5;
}

void Heliostat::setTarget(const Vector2 &target)
{
    Heliostat::target = target;
}

const Vector2 &Heliostat::getTargetSet() const
{
    return targetSet;
}

void Heliostat::setTargetSet(const Vector2 &targetSet)
{
    Heliostat::targetSet = targetSet;
}

void Heliostat::updateHeliostatMovement(double dt, double cellw, double cellh)
{
    Vector2 moveDir = targetSet.subtract(target);
    double len = moveDir.length();
    if (len <= 0.25)
    {
        // nothing to do
        return;
    }
    double speed = std::min(len, 1.0);
    Vector2 offset = moveDir.normalize().multiply(speed);
    target = target.add(offset);
}

