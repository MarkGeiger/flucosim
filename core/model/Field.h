#ifndef CORE_FIELD_H
#define CORE_FIELD_H

#include <vector>
#include "Heliostat.h"
#include "Receiver.h"

class Field
{

private:
    Receiver receiver;
    std::vector<Heliostat> heliostats{};
    Vector3 sunVector = Vector3(0, 0, 1);

    void generateReceiver();
    void generateHeliostats();

public:
    Field();

    std::vector<Heliostat> &getHeliostats();
    Receiver &getReceiver();
    const Vector3 &getSunVector() const;

};


#endif //CORE_FIELD_H