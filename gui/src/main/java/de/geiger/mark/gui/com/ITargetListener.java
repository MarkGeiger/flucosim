package de.geiger.mark.gui.com;

import java.util.List;

public interface ITargetListener
{
    void onNewTargets(List<Double> x, List<Double> y);
}
