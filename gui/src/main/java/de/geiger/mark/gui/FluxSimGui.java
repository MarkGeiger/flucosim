package de.geiger.mark.gui;

import de.geiger.mark.gui.vis.MainFrame;

public class FluxSimGui
{
    public static void main(String[] args)
    {
        MainFrame mainPanel = new MainFrame();
        mainPanel.setVisible(true);
    }
}
