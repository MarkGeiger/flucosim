package de.geiger.mark.gui.vis;

import de.geiger.mark.gui.com.UDPListener;
import de.geiger.mark.gui.com.UDPSend;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class MainFrame extends JFrame
{

    private int snapID = 0;

    public MainFrame()
    {
        setSize(1600, 700);
        setTitle("FluxControlSimulator GUI");
        setLayout(new BorderLayout());

        JPanel fluxPanels = new JPanel();
        fluxPanels.setLayout(new BoxLayout(fluxPanels, BoxLayout.X_AXIS));
        FluxPanel mp = new FluxPanel("flux", "Flux on Receiver", "[kW / m²]", true, -1);
        FluxPanel dif = new FluxPanel("dif", "Flux difference after 1s", "[kW / m²]", true , -1);
        FluxPanel det = new FluxPanel("det", "Detected movement raw", "[kW / m²]", false, 50);
        FluxPanel model = new FluxPanel("model", "LWPR - model output", "[path]", false, 250);
        fluxPanels.add(mp);
        fluxPanels.add(dif);
        fluxPanels.add(det);
        fluxPanels.add(model);

        UDPListener com = new UDPListener(mp, dif, det, model, model);

        JPanel controlPanel = new JPanel();
        controlPanel.setLayout(new BorderLayout());

        JButton shuffel = new JButton("shuffel");
        JTextField shuffelField = new JTextField("20");
        controlPanel.add(shuffel, BorderLayout.SOUTH);
        controlPanel.add(shuffelField, BorderLayout.CENTER);

        JButton snap = new JButton("take Snap");
        snap.addActionListener(actionEvent ->
        {
            mp.setTakeSnapshot(true);
            dif.setTakeSnapshot(true);
            det.setTakeSnapshot(true);
            model.setTakeSnapshot(true);
            BufferedImage img1 = mp.getLastImage();
            BufferedImage img2 = dif.getLastImage();
            BufferedImage img3 = det.getLastImage();
            BufferedImage img4 = model.getLastImage();

            BufferedImage combinedImage = new BufferedImage(img1.getWidth()*4, img1.getHeight(), BufferedImage.TYPE_INT_RGB);
            combinedImage.getGraphics().drawImage(img1, 0,0,null);
            combinedImage.getGraphics().drawImage(img2, img1.getWidth(),0,null);
            combinedImage.getGraphics().drawImage(img3, img1.getWidth() + img2.getWidth(),0,null);
            combinedImage.getGraphics().drawImage(img4, img1.getWidth() + img2.getWidth() + img3.getWidth(),0,null);

            String pathPrefix = "combined";
            File dir = new File("./snaps/" + pathPrefix);
            if (dir.mkdirs())
            {
                System.out.println("created subfolder:" + dir.getAbsolutePath());
            }
            String idString = String.format("%04d", snapID);
            String path = "./snaps/" + pathPrefix + "/" + idString + ".png";
            File file = new File(path);
            System.out.println("taking snap store to: " + file.getAbsolutePath());
            try
            {
                ImageIO.write(combinedImage, "png", file);
                snapID++;
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        });
        add(snap, BorderLayout.NORTH);

        shuffel.addActionListener(actionEvent -> UDPSend.sendRandomShuffel(Integer.parseInt(shuffelField.getText())));
        add(fluxPanels, BorderLayout.CENTER);
        add(controlPanel, BorderLayout.SOUTH);


        addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
                super.windowClosing(e);
                com.close();
                try
                {
                    Thread.sleep(2);
                } catch (InterruptedException ex)
                {
                    ex.printStackTrace();
                }
                e.getWindow().dispose();
            }
        });
    }
}
