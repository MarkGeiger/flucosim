package de.geiger.mark.gui.vis;

import de.geiger.mark.gui.com.IFluxCommand;
import de.geiger.mark.gui.com.ITargetListener;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class FluxPanel extends JPanel implements IFluxCommand, ITargetListener
{

    private double[] flux = new double[100 * 100];
    private List<Double> targetX;
    private List<Double> targetY;
    private int dimx = 20;
    private int dimy = 20;

    private boolean takeSnapshot = false;
    private int snapID = 0;

    private String pathPrefix;
    private String name;
    private String unit;
    private boolean showSidebar;
    private int fixedMaxVal;

    private BufferedImage lastImage;


    public FluxPanel(String pathPrefix, String name, String unit, boolean showSidebar, int fixedMaxVal)
    {
        this.pathPrefix = pathPrefix;
        this.name = name;
        this.unit = unit;
        this.showSidebar = showSidebar;
        this.fixedMaxVal = fixedMaxVal;
        setBorder(BorderFactory.createBevelBorder(1));
    }

    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        if (getWidth() <= 300)
        {
            return;
        }

        int sidebarW = Math.min(80, getWidth() / 4);
        if (!showSidebar)
        {
            sidebarW = 0;
        }
        int width = getWidth() - sidebarW;
        int height = getHeight() - 30;
        BufferedImage awtImage = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = (Graphics2D) awtImage.getGraphics();

        double cellw = width / dimx;
        double cellh = height / dimy;

        double neededW = cellw * dimx;
        double neededH = cellh * dimy;

        int offX = (int) ((width - neededW) / 2);
        int offY = (int) ((height - neededH) / 2);

        double maxVal = 0;
        for (int j = 0; j < dimy; j++)
        {
            for (int i = 0; i < dimx; i++)
            {
                double v = flux[i + j * dimy];
                if (v > maxVal)
                {
                    maxVal = v;
                }
            }
        }
        for (int j = 0; j < dimy; j++)
        {
            for (int i = 0; i < dimx; i++)
            {
                double val = flux[i + j * dimy];
                float color = (float) (1 - Math.min(1, Math.max(0, (val / maxVal))));
                g2.setColor(Color.getHSBColor(color, 1f, 1 - color));
                int x = (int) (i * cellw) + offX;
                int y = (int) (j * cellh) + offY;
                g2.fillRect(x, y, (int) cellw, (int) cellh);
            }
        }

        if (targetX != null)
        {
            for (int i = 0; i < targetX.size(); i++)
            {
                int tx = (int) Math.round(targetX.get(i));
                int ty = (int) Math.round(targetY.get(i));
                g2.setColor(Color.WHITE);
                int x = (int) ((int) (tx * cellw) + offX + cellw);
                int y = (int) ((int) (ty * cellh) + offY + cellh);
                g2.fillRect(x, y, (int) cellw, (int) cellh);


                if (i == targetX.size() -1 )
                {
                    int w = (int) (cellw*20);
                    int h = (int) (cellh*20);
                    g2.drawOval(x - w/2, y - h/2, w, h);
                }
            }
        }

        g2.setColor(new Color(198, 195, 195, 255));
        g2.fillRect(width, 0, sidebarW, height);

        int barW = Math.max(20, sidebarW - 60);
        int sx = width + 10;
        int sy = 20;
        int h = height - 40;
        int steps = 0;
        for (int j = sy; j < sy + h; j++)
        {
            double percentage = (steps / ((double) height));
            float val = (float) percentage;
            Color color = Color.getHSBColor(val, 1f, 1 - val);
            g2.setColor(color);
            for (int i = sx; i < sx + barW; i++)
            {
                g2.drawLine(i, j, i, j);
            }
            steps++;
        }

        // draw line numbers
        steps = 0;
        g2.drawString(unit, sx + barW - 27, sy - 5);
        for (int j = sy; j < sy + h; j++)
        {
            double percentage = (steps / ((double) height));
            if (steps % 100 == 0)
            {
                g2.setColor(Color.BLACK);
                g2.drawLine(sx + barW - 5, h - j + sy*2-1, sx + barW + 5, h - j + sy*2-1);
                String val = "" + (int) (maxVal * percentage);
                g2.drawString(val, sx + barW + 10, h - j + sy*2-1 + 5);
            }
            steps++;
        }

        g2.setColor(new Color(198, 195, 195, 255));
        g2.fillRect(0,height, getWidth(), getHeight());
        g2.setColor(Color.BLACK);
        g2.drawString(name, 100, height + 17);

        // copy bufferedImage to drawing area
        Graphics2D drawingArea = (Graphics2D) g;
        drawingArea.drawImage(awtImage, 0, 0, null);
        lastImage = awtImage;

        if (takeSnapshot)
        {
            takeSnapshot = false;
            File dir = new File("./snaps/" + pathPrefix);
            if (dir.mkdirs())
            {
                System.out.println("created subfolder:" + dir.getAbsolutePath());
            }
            String idString = String.format("%04d", snapID);
            String path = "./snaps/" + pathPrefix + "/" + idString + ".png";
            File file = new File(path);
            System.out.println("taking snap store to: " + file.getAbsolutePath());
            try
            {
                ImageIO.write(awtImage, "png", file);
                snapID++;
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onNewFluxMap(double[] flux, int dimx, int dimy)
    {
        this.flux = flux;
        this.dimx = dimx;
        this.dimy = dimy;
        repaint();
    }

    public void setTakeSnapshot(boolean takeSnapshot)
    {
        this.takeSnapshot = takeSnapshot;
    }

    public BufferedImage getLastImage()
    {
        return lastImage;
    }

    @Override
    public void onNewTargets(List<Double> x, List<Double> y)
    {
        this.targetX = x;
        this.targetY = y;
    }
}
