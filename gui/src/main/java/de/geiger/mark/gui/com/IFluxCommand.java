package de.geiger.mark.gui.com;

public interface IFluxCommand
{
    void onNewFluxMap(double[] flux, int dimx, int dimy);
}
