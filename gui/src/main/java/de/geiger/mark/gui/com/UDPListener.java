package de.geiger.mark.gui.com;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

public class UDPListener
{
    private double[] flux = new double[100 * 100];
    private double[] fluxDif = new double[100 * 100];
    private double[] fluxDet = new double[100 * 100];
    private double[] fluxModel = new double[100 * 100];

    private List<Double> listX = new ArrayList<>();
    private List<Double> listY = new ArrayList<>();

    private boolean running = true;
    private IFluxCommand fluxListener;
    private IFluxCommand fluxDifListener;
    private IFluxCommand detListener;
    private IFluxCommand modelListener;
    private ITargetListener targetListener;

    public UDPListener(IFluxCommand listener, IFluxCommand diflistener, IFluxCommand detListener, IFluxCommand modelListener, ITargetListener targetListener)
    {
        Thread t = new Thread(this::listen);
        t.start();
        this.fluxListener = listener;
        this.fluxDifListener = diflistener;
        this.detListener = detListener;
        this.modelListener = modelListener;
        this.targetListener = targetListener;
    }

    public void close()
    {
        this.running = false;
    }

    private void listen()
    {
        try
        {
            int port = 6003;
            DatagramSocket dsocket = new DatagramSocket(port);
            byte[] buffer = new byte[50000];

            // Create a packet to receive data into the buffer
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length);

            // Now loop forever, waiting to receive packets and printing them.
            while (running)
            {
                // Wait to receive a datagram
                dsocket.receive(packet);

                ByteBuffer conv = ByteBuffer.wrap(packet.getData());
                conv.order(ByteOrder.LITTLE_ENDIAN);
                int payloadLen = packet.getLength();

                if (!parseCommand(conv, payloadLen))
                {
                    System.out.println("Could not parse udp command");
                }

                // Reset the length of the packet before reusing it.
                packet.setLength(buffer.length);
            }
            System.out.println("exiting UDP com");
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private boolean parseCommand(ByteBuffer conv, int payloadLen)
    {
        int id = conv.getInt();
        if (id == 5)
        {
            // target points
            int numOfTargets = conv.getInt();
            //listX.clear();
            //listY.clear();
            for (int i = 0; i < numOfTargets; i++)
            {
                double x = conv.getDouble();
                double y = conv.getDouble();
                listX.add(x);
                listY.add(y);
            }
            targetListener.onNewTargets(listX, listY);
        }
        if (id == 1 || id == 2 || id == 3 || id == 4)
        {
            int dimx = conv.getInt();
            int dimy = conv.getInt();
            int expectedSize = dimx * dimy * Double.BYTES + Integer.BYTES * 3;

            if (expectedSize != payloadLen)
            {
                System.out.println("Broken flux packet: " + expectedSize + " != " + payloadLen);
                return false;
            }
            if (id == 1)
            {
                for (int j = 0; j < dimy; j++)
                {
                    for (int i = 0; i < dimx; i++)
                    {
                        flux[i + j * dimy] = conv.getDouble();
                    }
                }
                fluxListener.onNewFluxMap(flux, dimx, dimy);
            } else if (id == 2)
            {
                for (int j = 0; j < dimy; j++)
                {
                    for (int i = 0; i < dimx; i++)
                    {
                        fluxDif[i + j * dimy] = conv.getDouble();
                    }
                }
                fluxDifListener.onNewFluxMap(fluxDif, dimx, dimy);
            } else if (id == 3)
            {
                for (int j = 0; j < dimy; j++)
                {
                    for (int i = 0; i < dimx; i++)
                    {
                        fluxDet[i + j * dimy] = conv.getDouble();
                    }
                }
                detListener.onNewFluxMap(fluxDet, dimx, dimy);
            } else {
                for (int j = 0; j < dimy; j++)
                {
                    for (int i = 0; i < dimx; i++)
                    {
                        fluxModel[i + j * dimy] = conv.getDouble();
                    }
                }
                modelListener.onNewFluxMap(fluxModel, dimx, dimy);
            }
        }

        return true;
    }
}
