package de.geiger.mark.gui.com;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.ByteBuffer;

public class UDPSend
{
    private static void send(byte[] bytes)
    {
        try
        {
            DatagramSocket serverSocket = new DatagramSocket();
            InetAddress IPAddress = InetAddress.getByName("127.0.0.1");
            DatagramPacket sendPacket = new DatagramPacket(bytes, bytes.length, IPAddress, 6004);
            serverSocket.send(sendPacket);
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void sendRandomShuffel(int numToShuffel)
    {
        ByteBuffer buffer = ByteBuffer.allocate(Integer.BYTES*2);
        buffer.putInt(1);
        buffer.putInt(numToShuffel);
        send(buffer.array());
    }
}
